# Installation recipe for HostConfiguration

### 1. Make sure that you have installed following components
- Tango 9.2.2
- Java >= 1.8
- Python 2.7 and Python 3.5
- Numpy, PyTango
- Taurus
- MariaDB 10.2
  
### 2. Setup environment variables providing credentials to access MariaDB (if you haven't done i yet)
1. Open command line
2. Invoke commands:
    - **cd %TANGO_ROOT%\bin**
    - **dbconfig.exe**
        
    You should see following window:
        
    ![Database config](screens/database_config.png)
    
3. Enter username and password. (the same you have chosen while installing MariaDB)
4. Execute changes by clicking "Create" button.

### 3. Initialize Tango database (if you haven't done it yet)
1. Open command line
2. Add PATH to MySQL client: **set PATH=%PATH%;"C:\Program Files\MariaDB 10.2\bin"**
3. Go to following directory: **cd %TANGO_ROOT%\share\tango\db**
4. Execute script: **create_db.bat**

### 4. Start Databaseds Device Server
1. Go to following directory: **cd %TANGO_ROOT%\bin**
2. Start Databaseds: **start-db.bat**
    
    You should see following statement: *Ready to accept request*

### 5. Make Databaseds run as a service
1. Download [NSSM](http://nssm.cc/release/nssm-2.24.zip)
2. Unpack files to desirable location, e.g. %TANGO_ROOT%\bin
3. Go to NSSM directory: **cd %TANGO_ROOT%\bin\nssm-2.24\win64**
4. Install Databaseds service: **nssm.exe install Tango-DataBaseds**
    - Complete *Path* and *Startup directory* to fit your installation
    - In arguments provide: **2 -ORBendPoint giop:tcp::10000**
        
        Note that *10000* is your TANGO_HOST port. In your installation it can be different.
    
    ![NSSM_step1](screens/nssm_step1.png)
    
    - Go to *Environment* tab and provide correct *MYSQL_USER* and *MYSQL_PASSWORD* variables
    
    ![NSSM_step2](screens/nssm_step2.png)
    
    - Click *Install service* button.
      
      In command line you should see following statement: *Service "Tango-DataBaseds" installed successfully!*
    - Start service: **nssm.exe start Tango-DataBaseds**
      
      If everything is correct, you should see this statement: *Tango-DataBaseds: START: The operation completed successfully.*
 5. To make sure that everything went ok, you can run Astor:
    - Go to bin directory: **cd %TANGO_ROOT%\bin**
    - Start Astor: **start-astor.bat**
      You should see similar window to this:
      
      ![Astor](screens/astor.png)

### 6. Congratulations. You have successfully configured your Tango Host!
